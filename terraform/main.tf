resource "azurerm_resource_group" "EKSgroup" {
  name     = "EKSgroup"
  location = "East US"
}

resource "azurerm_kubernetes_cluster" "ekscluster" {
  name                = "ekscluster"
  location            = azurerm_resource_group.EKSgroup.location
  resource_group_name = azurerm_resource_group.EKSgroup.name
  dns_prefix          = "ekscluster"

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_D2_v2"
  }

  identity {
    type = "SystemAssigned"
  }

  tags = {
    Environment = "Testing"
  }
}

output "client_certificate" {
  value = azurerm_kubernetes_cluster.ekscluster.kube_config.0.client_certificate
}

